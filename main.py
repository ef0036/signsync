import argparse
import cv2
import numpy as np
import subprocess
import os

def detect_light_change(video_path, threshold=800000, max_frames=4500):
    """Detect the first frame with a significant change in lighting."""
    cap = cv2.VideoCapture(video_path)
    if not cap.isOpened():
        raise ValueError(f"Error opening video file: {video_path}")
    
    frame_count = 0
    ret, frame = cap.read()
    if not ret:
        raise ValueError(f"Error reading the first frame of {video_path}")
    roi_x, roi_y = 0, 0
    roi_width = int(frame.shape[1] * 0.25)
    roi_height = int(frame.shape[0] * 0.25)
    previous_frame = frame[roi_y:roi_y + roi_height, roi_x:roi_x + roi_width]
    light_change_frame = None

    while ret and frame_count < max_frames:
        roi_frame = frame[roi_y:roi_y + roi_height, roi_x:roi_x + roi_width]
        diff = cv2.absdiff(roi_frame, previous_frame)
        diff_sum = np.sum(diff)
        if diff_sum > threshold:
            light_change_frame = frame_count
            print(f"Significant light change detected in {video_path} at frame {frame_count} with pixel change sum {diff_sum}")
            break
        previous_frame = roi_frame.copy()
        ret, frame = cap.read()
        frame_count += 1

    cap.release()
    if light_change_frame is None:
        print(f"No significant light change detected in the top left corner of {video_path} within the first {max_frames} frames.")
    return light_change_frame

def get_video_info(video_path):
    """Retrieve frame rate, width, height, and total duration of the video."""
    cap = cv2.VideoCapture(video_path)
    if not cap.isOpened():
        raise ValueError(f"Unable to open video {video_path}")
    frame_rate = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    duration = num_frames / frame_rate
    cap.release()
    return frame_rate, width, height, duration

def adjust_video_duration(input_path, output_path, target_duration):
    """Adjust the video speed to match the specified duration using H.264 codec."""
    frame_rate, _, _, input_duration = get_video_info(input_path)
    speed_factor = input_duration / target_duration
    command = [
    'ffmpeg', '-i', input_path,
    '-filter_complex', f"setpts=PTS/({input_duration/target_duration})", '-r', '57', '-b:v', '10M', '-qp', '18', output_path
    ]
    run_ffmpeg_command(command, f"Adjusting playback speed of {input_path} to match duration {target_duration} seconds")

def combine_videos_side_by_side(trimmed_path1, trimmed_path2, output_path, frame_rate, temp_files):
    """Combine two trimmed videos side-by-side."""
    _, width1, height1, _ = get_video_info(trimmed_path1)
    _, width2, height2, _ = get_video_info(trimmed_path2)

    # Scale the height of the second video to match the first video
    if height1 != height2:
        temp_scaled_path = 'temp_scaled.mp4'
        command = [
            'ffmpeg', '-y',
            '-i', trimmed_path2,
            '-vf', f'scale=-1:{height1}',
            '-b:v', '10M', '-qp', '18',
            temp_scaled_path
        ]
        run_ffmpeg_command(command, f"Scaling height of {trimmed_path2} to match {trimmed_path1}")
        temp_files.append(temp_scaled_path)
        trimmed_path2 = temp_scaled_path
    
    # Get the new width of the second video after scaling
    _, new_width2, new_height2, _ = get_video_info(trimmed_path2)
    
    # Crop the width of the second video to match the first video
    if new_width2 > width1:
        temp_cropped_path = 'temp_cropped.mp4'
        crop_width = width1
        crop_x = (new_width2 - crop_width) // 2
        command = [
            'ffmpeg', '-y',
            '-i', trimmed_path2,
            '-vf', f'crop={crop_width}:{new_height2}:{crop_x}:0',
            '-b:v', '10M', '-qp', '18',
            temp_cropped_path
        ]
        run_ffmpeg_command(command, f"Cropping width of {trimmed_path2} to match {trimmed_path1}")
        temp_files.append(temp_cropped_path)
        trimmed_path2 = temp_cropped_path
    
    command = [
        'ffmpeg', '-y',
        '-i', trimmed_path1, '-i', trimmed_path2,
        '-filter_complex', f'[0:v]fps=fps={frame_rate}[left];[1:v]fps=fps={frame_rate}[right];[left][right]hstack=inputs=2[v]',
        '-map', '[v]', '-c:v', 'h264_nvenc', '-preset', 'fast', '-b:v', '10M', '-qp', '18',
        output_path
    ]
    run_ffmpeg_command(command, f"Combining {trimmed_path1} and {trimmed_path2} into {output_path}")

def trim_video(video_path, output_path, start_frame, frame_rate, trim_end=None):
    """Trim the video from the specified start frame to an optional duration using NVIDIA NVENC H.264 codec."""
    print(f"trimming {video_path}")
    start_time = start_frame / frame_rate
    command = [
        'ffmpeg', '-y', '-i', video_path,
        '-ss', str(start_time),  # Start time in seconds
    ]
    if trim_end is not None:
        duration = get_video_info(video_path)[3] - start_time - trim_end
        if duration > 0:
            command.extend(['-t', str(duration)])  # Duration in seconds
    command.extend([
        '-c:v', 'h264_nvenc',  # Use NVENC for h.264 encoding
        '-preset', 'fast', '-b:v', '10M', '-qp', '18',
        output_path
    ])
    run_ffmpeg_command(command, f"Trimming {video_path} from start time {start_time} with {trim_end} seconds trimmed from the end")

def run_ffmpeg_command(command, description):
    """Execute an FFmpeg command and handle the output."""
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    stdout, stderr = process.communicate()
    if process.returncode != 0:
        print(f"Error during {description}: {stderr}")
    else:
        print(f"{description} completed successfully: {stdout}")

def main(video_path1, video_path2, output_filename, trim_end):
    temp_files = []
    try:
        # Detect light changes and trim videos
        frame_rate1, _, _, duration1 = get_video_info(video_path1)
        frame_rate2, _, _, duration2 = get_video_info(video_path2)

        light_change_frame1 = detect_light_change(video_path1)
        light_change_frame2 = detect_light_change(video_path2)

        trimmed_path1 = f"{video_path1[:-4]}_trimmed.mp4"
        trimmed_path2 = f"{video_path2[:-4]}_trimmed.mp4"

        if light_change_frame1 is not None:
            trim_video(video_path1, trimmed_path1, light_change_frame1, frame_rate1)
        else:
            trimmed_path1 = video_path1

        if light_change_frame2 is not None:
            trim_video(video_path2, trimmed_path2, light_change_frame2, frame_rate2)
        else:
            trimmed_path2 = video_path2

        temp_files.append(trimmed_path1)
        temp_files.append(trimmed_path2)
        
        # Trim the end only for the longer video
        if duration1 > duration2:
            trim_video(trimmed_path1, "temp_trimmed_1.mp4", 0, frame_rate1, trim_end)
            trimmed_path1 = "temp_trimmed_1.mp4"
        else:
            trim_video(trimmed_path2, "temp_trimmed_2.mp4", 0, frame_rate2, trim_end)
            trimmed_path2 = "temp_trimmed_2.mp4"

        temp_files.append(trimmed_path1)
        temp_files.append(trimmed_path2)

        fr, _, _, duration1 = get_video_info(trimmed_path1)
        _, _, _, duration2 = get_video_info(trimmed_path2)
        temp_out_1 = "temp_out_1.mp4"
        temp_out_2 = "temp_out_2.mp4"

        if duration1 > duration2:
            adjust_video_duration(trimmed_path1, temp_out_1, duration2)
            temp_out_2 = trimmed_path2
        else:
            adjust_video_duration(trimmed_path2, temp_out_2, duration1)
            temp_out_1 = trimmed_path1

        temp_files.append(temp_out_1)
        temp_files.append(temp_out_2)

        combine_videos_side_by_side(temp_out_1, temp_out_2, output_filename, 57, temp_files)
    finally:
        # Cleanup temporary files
        for temp_file in temp_files:
            if os.path.exists(temp_file):
                os.remove(temp_file)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process and combine two videos side by side.")
    parser.add_argument('--video_path1', type=str, default='data/1.mp4', help='Path to the first video file')
    parser.add_argument('--video_path2', type=str, default='data/2.mp4', help='Path to the second video file')
    parser.add_argument('--output_filename', type=str, default='out.mp4', help='Filename for the output video')
    parser.add_argument('--trim_end', type=float, default=9.5, help='Number of seconds to be trimmed from the end of the longer video')

    args = parser.parse_args()

    main(args.video_path1, args.video_path2, args.output_filename, args.trim_end)
