
# Video Processing and Combination Tool

This tool processes and combines two videos side by side. It detects significant light changes, trims the videos from the detected light change points, adjusts their durations, and then combines them into a single video.

## Features

- **Light Change Detection**: Detects significant changes in lighting in the videos to determine trimming points.
- **Video Trimming**: Trims the videos from the detected points and removes a specified duration from the end of the longer video.
- **Duration Adjustment**: Adjusts the playback speed of videos to ensure both have the same duration before combining.
- **Side-by-Side Combination**: Combines the two videos side by side into a single output video.

## Requirements

- Python 3.x
- OpenCV
- NumPy
- FFmpeg (with NVIDIA NVENC support for GPU acceleration)
- argparse

## Installation

1. Install Python 3.x if you don't have it already.
2. Install the required Python libraries:
    \`\`\`bash
    pip install opencv-python-headless numpy
    \`\`\`
3. Install FFmpeg with NVIDIA NVENC support. Follow the [FFmpeg installation guide](https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu) for your operating system.

## Usage

Run the script with the following command:

```bash
python video_processing.py --video_path1 PATH_TO_FIRST_VIDEO --video_path2 PATH_TO_SECOND_VIDEO --output_filename OUTPUT_FILENAME --trim_end SECONDS_TO_TRIM_FROM_END_OF_LONGEST_VIDEO
```

### Arguments

- `--video_path1`: Path to the first video file (default: `data/1.mp4`).
- `--video_path2`: Path to the second video file (default: `data/2.mp4`).
- `--output_filename`: Filename for the output video (default: `out.mp4`).
- `--trim_end`: Number of seconds to be trimmed from the end of the longer video (default: `9.5`).

### Example

```bash
python video_processing.py --video_path1 data/1.mp4 --video_path2 data/2.mp4 --output_filename combined_output.mp4 --trim_end 10
```

## Functions

### `detect_light_change(video_path, threshold=800000, max_frames=4500)`

Detects the first frame with a significant change in lighting.

### `get_video_info(video_path)`

Retrieves frame rate, width, height, and total duration of the video.

### `adjust_video_duration(input_path, output_path, target_duration)`

Adjusts the video speed to match the specified duration using H.264 codec.

### `combine_videos_side_by_side(trimmed_path1, trimmed_path2, output_path, frame_rate)`

Combines two trimmed videos side-by-side.

### `trim_video(video_path, output_path, start_frame, frame_rate, trim_end=None)`

Trims the video from the specified start frame and removes the specified duration from the end if \`trim_end\` is provided.

### `run_ffmpeg_command(command, description)`

Executes an FFmpeg command and handles the output.

### `main(video_path1, video_path2, output_filename, trim_end)`

Main function to process and combine the videos.
